
package com.sensor.SensorProject;

import com.sensor.SensorProject.repository.SensorRepository;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
class SensorProjectApplicationTests {
	private static Logger logger = LoggerFactory.getLogger(SensorProjectApplicationTests.class);

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private SensorRepository sensorRepository;

	private String token;

	@BeforeEach
	public void setupBeforeTests() throws Exception{
		sensorRepository.deleteAll();
		MvcResult result = this.mockMvc.perform(post("/token")
				.param("user", "pippo")
				.param("password", "1234"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content").exists())
				.andReturn();

		this.token = new JacksonJsonParser().parseMap(result.getResponse().getContentAsString()).get("content").toString();
	}


	@Test
	public void shouldReturnRepositoryIndex() throws Exception {
		this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(
				jsonPath("$._links.sensors").exists());
	}

	@Test
	public void shouldCreateEntity() throws Exception {
		sensorRepository.deleteAll();
		MvcResult result = this.mockMvc.perform(post("/token")
				.param("user", "pippo")
				.param("password", "1234"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content").exists())
				.andReturn();

		this.token = new JacksonJsonParser().parseMap(result.getResponse().getContentAsString()).get("content").toString();

		this.mockMvc.perform(post("/sensors")
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Frodo\", \"type\":\"test\", \"value\":\"5\"}"))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("sensors/")));
	}

	@Test
	public void shouldRetrieveEntity() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(post("/sensors")
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Frodo\", \"type\":\"test\", \"value\":\"5\"}"))
				.andExpect(status().isCreated()).andReturn();

		String location = mvcResult.getResponse().getHeader("Location");
		this.mockMvc.perform(get(location)).andExpect(status().isOk()).andExpect(
				jsonPath("$.name").value("Frodo")).andExpect(
				jsonPath("$.type").value("test")).andExpect(
				jsonPath("$.value").value(5));
	}

	@Test
	public void shouldQueryEntity() throws Exception {
		this.mockMvc.perform(post("/sensors")
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Frodo\", \"type\":\"test\", \"value\":\"5\"}"))
				.andExpect(status().is(201));

		this.mockMvc.perform(get("/sensors/search/findByType?type={type}", "test"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.sensors[0].name").value("Frodo"));
	}

	@Test
	public void shouldUpdateEntity() throws Exception {

		MvcResult mvcResult = this.mockMvc.perform(post("/sensors")
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Frodo\", \"type\":\"test\", \"value\":\"5\"}"))
				.andExpect(status().isCreated()).andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		this.mockMvc.perform(put(location)
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Bilbo\", \"type\":\"test\", \"value\":\"6\"}"))
				.andExpect(status().isNoContent());

		this.mockMvc.perform(get(location))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("Bilbo"))
				.andExpect(jsonPath("$.value").value(6));
	}

	@Test
	public void shouldPartiallyUpdateEntity() throws Exception {

		MvcResult mvcResult = this.mockMvc.perform(post("/sensors")
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Frodo\", \"type\":\"test\", \"value\":\"5\"}"))
				.andExpect(status().isCreated()).andReturn();

		String location = mvcResult.getResponse().getHeader("Location");

		this.mockMvc.perform(patch(location)
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Bilbo Jr.\"}"))
				.andExpect(status().isNoContent());

		this.mockMvc.perform(get(location))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("Bilbo Jr."))
				.andExpect(jsonPath("$.value").value(5));
	}

	@Test
	public void shouldDeleteEntity() throws Exception {

		MvcResult mvcResult = this.mockMvc.perform(post("/sensors")
				.header("Authorization", "Bearer " + this.token)
				.content("{\"name\": \"Frodo\", \"type\":\"test\", \"value\":\"5\"}"))
				.andExpect(status().isCreated()).andReturn();

		String location = mvcResult.getResponse().getHeader("Location");
		this.mockMvc.perform(delete(location).header("Authorization", "Bearer " + this.token))
				.andExpect(status().isNoContent());
		this.mockMvc.perform(get(location)).andExpect(status().isNotFound());
	}
}
