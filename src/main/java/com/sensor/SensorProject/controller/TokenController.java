package com.sensor.SensorProject.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.Date;

@RestController
public class TokenController {
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public EntityModel<?> login(@RequestParam String user, @RequestParam String password) {
// ATTENTION: here we should check user and password
        return new EntityModel<>(
                Jwts.builder().setSubject(user)
                        .claim("roles", "guest") // set custom claim
                        .setIssuedAt(new Date()) // set issue date
                        .setExpiration(Date.from(Instant.now().plusSeconds(60))) // set expiration
                        .signWith(SignatureAlgorithm.HS256, "secret").compact()); // set algo signature
    }
}