package com.sensor.SensorProject.client;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.apache.http.entity.ContentType;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Client {

    //private static final String SERVER_URL = "http://localhost:8080/";
    private static final String SERVER_URL = "http://35.203.136.92:80/back/";
    private static final Random rand = new Random();

    public static void main(String args[]) throws InterruptedException, UnirestException {
        //Unirest.config().verifySsl(false);
        HttpResponse<JsonNode> tokenResponse = Unirest
                .post(SERVER_URL + "token")
                .header("Content-Type", ContentType.APPLICATION_FORM_URLENCODED.getMimeType())
                .field("user", "mellacsi")
                .field("password", "1234").asJson();
        String token = tokenResponse.getBody().getObject().get("content").toString();

        Double value = 25d;
        for (int i = 0; i < 5; i++) {
            int prod = (rand.nextInt() < 0 ) ? -1 : 1;
            if(i % 2 == 0) value = value + (5 * prod) ;
            value = (rand.nextDouble() * prod) + value;

            Unirest.post(SERVER_URL+"sensors")
                    .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
                    .header("Authorization", "Bearer " + token)
                    .body("{\"name\":\"sensor" + i + "\", \"type\":\"temp\", \"value\":"+value+"}")
                    .asJson();
            Thread.sleep(1000);
        }
    }
}
