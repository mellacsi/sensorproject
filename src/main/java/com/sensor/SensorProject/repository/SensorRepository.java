package com.sensor.SensorProject.repository;

import com.sensor.SensorProject.model.Sensor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SensorRepository extends PagingAndSortingRepository<Sensor, Long> {
    // On /sensors/search you can see these methods
    List<Sensor> findByType(@Param("type") String type);
}
