package com.sensor.SensorProject;

import com.sensor.SensorProject.filter.JwtFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

//Before this app run docker run -e "MYSQL_DATABASE=sensor" -e "MYSQL_ROOT_PASSWORD=root" -p 3306:3306 mysql:5.6
@SpringBootApplication
public class SensorProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SensorProjectApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtFilter());
		registrationBean.addUrlPatterns("/sensors/*");
		return registrationBean;
	}

}
