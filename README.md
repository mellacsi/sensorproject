## Sensor project
Imagine you have to develop the back-end for a company that sells personal sensors (weather stations,
body scales, sleep tracking, blood pressure monitor, watches with activity tracking, ...).
The aim of the REST API that your going to develop is to have a web service that can handle all operations
for a single type of sensor :
- upload sensor data
- retrieve all sensors data
- update sensor data
- delete sensor data