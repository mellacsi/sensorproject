# Use maven build image.
FROM maven:3.3-jdk-8 as build
WORKDIR /app

# Copy local code to the container image.
COPY . ./
WORKDIR /app

# Build a release artifact.
RUN mvn package

# Use maven build image.
# FROM openjdk:8 as runtime
# WORKDIR /app
# COPY --from=build app/target ./
# Run the web service on container startup.
# ENTRYPOINT ["java","-jar","/app/sensor-rest-0.0.1-SNAPSHOT.jar"]